package id.ac.prasetyo.dendy.appx8

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class ActivitySetting : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    var BG_TITLE_COLOR = "bg_title_color"
    var FONT_TITLE_COLOR = "font_title_color"
    var FONT_TITLE_SIZE = "font_title__size"
    var TITLE_TEXT = "title_text"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var DETAIL_TEXT = "detail_text"
    val DEF_TITLE_COLOR = "BLUE"
    val DEF_FONT_TITLE_COLOR  = "WHITE"
    val DEF_FONT_TITLE_SIZE = 24
    val DEF_TITLE_TEXT = "-------1917-------"
    val DEF_DETAIL_FONT_SIZE = 18
    val DEF_DETAIL_TEXT ="1917 adalah sebuah film perang dunia pertama"
    var FontTitleColor: String = ""
    var BGTitleColor: String = ""
    val arrayBGTitleColor = arrayOf("BLUE", "YELLOW", "GREEN", "BLACK")
    lateinit var adapterSpin: ArrayAdapter<String>


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_setting)
        btnSimpan.setOnClickListener(this)
        rgColor.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbBlue -> FontTitleColor = "BLUE"
                R.id.rbYellow -> FontTitleColor = "YELLOW"
                R.id.rbGreen -> FontTitleColor = "GREEN"
                R.id.rbBlack -> FontTitleColor = "BLACK"
            }
        }
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayBGTitleColor)
        sp1.adapter = adapterSpin
        sp1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                BGTitleColor = adapterSpin.getItem(position).toString()
            }
        }
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        var spinnerselection =
            adapterSpin.getPosition(preferences.getString(BG_TITLE_COLOR, DEF_TITLE_COLOR))
        sp1.setSelection(spinnerselection, true)
        var rgselection = preferences.getString(FONT_TITLE_COLOR, DEF_FONT_TITLE_COLOR)
        if (rgselection == "BLUE") {
            rgColor.check(R.id.rbBlue)
        } else if (rgselection == "YELLOW") {
            rgColor.check(R.id.rbYellow)
        } else if (rgselection == "GREEN") {
            rgColor.check(R.id.rbGreen)
        } else {
            rgColor.check(R.id.rbBlack)
        }
        sbTitle.progress = preferences.getInt(FONT_TITLE_SIZE, DEF_FONT_TITLE_SIZE)
        sbTitle.setOnSeekBarChangeListener(this)
        sbDetail.progress = preferences.getInt(DETAIL_FONT_SIZE, DEF_DETAIL_FONT_SIZE)
        sbDetail.setOnSeekBarChangeListener(this)
        edJudul.setText(preferences.getString(TITLE_TEXT, DEF_TITLE_TEXT))
        edDetail.setText(preferences.getString(DETAIL_TEXT, DEF_DETAIL_TEXT))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSimpan ->{
                preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(BG_TITLE_COLOR, BGTitleColor)
                prefEditor.putString(FONT_TITLE_COLOR, FontTitleColor)
                prefEditor.putInt(FONT_TITLE_SIZE, sbTitle.progress)
                prefEditor.putInt(DETAIL_FONT_SIZE, sbDetail.progress)
                prefEditor.putString(TITLE_TEXT, edJudul.text.toString())
                prefEditor.putString(DETAIL_TEXT, edDetail.text.toString())
                prefEditor.commit()
                Toast.makeText(this, "Perubahan Disimpan", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when (seekBar?.id) {
            R.id.sbTitle -> {

            }
            R.id.sbDetail -> {

            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }



}